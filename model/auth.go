package model

import (
	"net/http"
	"raccoon-api/api/input"
	"raccoon-api/core/api"
	"raccoon-api/core/log"
	"raccoon-api/dao/doc"
	"raccoon-api/model/core"
	"raccoon-api/util"

	"go.mongodb.org/mongo-driver/bson"
)

type Auth interface {
	MgrSignIn(in input.LoginWithCompany) (AuthUser, error)
}

type AuthUser interface {
	api.JwtDao
	input.ReqUser
}

func NewAuth(dbmodel core.MgoDBModel, log log.Logger) Auth {
	return &authImpl{
		dbmodel: dbmodel,
		log:     log,
	}
}

type authImpl struct {
	dbmodel core.MgoDBModel
	log     log.Logger
}

func (ai *authImpl) MgrSignIn(in input.LoginWithCompany) (AuthUser, error) {
	cp := util.MD5(in.Pwd)
	q := bson.M{"email": in.Account, "pwd": cp}
	u := doc.UserHasStoreAggr{}
	err := ai.dbmodel.PipeFindOne(&u, q)
	if err != nil {
		return nil, api.NewApiError(http.StatusUnauthorized, "user or password error")
	}
	if u.ID.IsZero() {
		return nil, api.NewApiError(http.StatusUnauthorized, "user or password error")
	}
	u.Vat = in.Vat

	u.AddLoginRecord(in.ClientInfo)
	_, err = ai.dbmodel.UpdateOne(&u, bson.D{
		{Key: "logininfo", Value: u.LoginInfo},
	}, &u)
	if err != nil {
		return nil, err
	}

	return &u, nil
}
