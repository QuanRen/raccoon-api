package core

import (
	"context"
	"raccoon-api/core/log"

	"github.com/olivere/elastic/v7"
)

type SearchModel interface {
	Ping(url string) int
}

func NewSearchModel(ctx context.Context, clt *elastic.Client, log log.Logger) SearchModel {
	return &searchModelImpl{
		client: clt,
		ctx:    ctx,
		log:    log,
	}
}

type searchModelImpl struct {
	client *elastic.Client

	log log.Logger
	ctx context.Context
}

func (sm *searchModelImpl) Ping(url string) int {
	_, i, _ := sm.client.Ping(url).Do(sm.ctx)
	return i
}
