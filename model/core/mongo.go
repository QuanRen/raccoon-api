package core

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"raccoon-api/core/db"
	"raccoon-api/util"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"raccoon-api/core/dao"
	"raccoon-api/core/log"
)

type MgoAggregate interface {
	GetPipeline(q bson.M) mongo.Pipeline
	GetC() string
}

type MgoDBModel interface {
	Save(d dao.DocInter, u dao.LogUser) (primitive.ObjectID, error)
	RemoveByID(d dao.DocInter, u dao.LogUser) (int64, error)
	UpdateOne(d dao.DocInter, fields bson.D, u dao.LogUser) (int64, error)
	FindByID(d dao.DocInter) error
	FindOne(d dao.DocInter, q bson.M) error
	Find(d dao.DocInter, q bson.M) (interface{}, error)
	PipeFindOne(aggr MgoAggregate, filter bson.M) error
	PipeFind(aggr MgoAggregate, filter bson.M) (interface{}, error)
	PagePipeFind(aggr MgoAggregate, filter bson.M, limit, page int64) (interface{}, error)
	PageFind(d dao.DocInter, q bson.M, limit, page int64) (interface{}, error)
	CountDocuments(d dao.DocInter, q bson.M) (int64, error)
	GetPaginationSource(d dao.DocInter, q bson.M) util.PaginationSource
}

func NewMgoModel(ctx context.Context, db *mongo.Database, log log.Logger) MgoDBModel {
	return &mgoModelImpl{
		db:  db,
		ctx: ctx,
		log: log,
	}
}

func NewMgoModelByReq(req *http.Request, source string) MgoDBModel {
	mgodb := db.GetCtxMgoDabase(req, source)
	if mgodb == nil {
		panic("database not set in req")
	}
	log := log.GetCtxLog(req)
	if log == nil {
		panic("log not set in req")
	}
	return &mgoModelImpl{
		db:  mgodb,
		ctx: req.Context(),
		log: log,
	}
}

func GetObjectID(id interface{}) (primitive.ObjectID, error) {
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		return primitive.ObjectIDFromHex(str)
	case "primitive.ObjectID":
		return id.(primitive.ObjectID), nil
	default:
		return primitive.NilObjectID, errors.New("not support type: " + dtype)
	}
}

type mgoModelImpl struct {
	db  *mongo.Database
	log log.Logger
	ctx context.Context

	indexExistMap map[string]bool
}

func (mm *mgoModelImpl) CountDocuments(d dao.DocInter, q bson.M) (int64, error) {
	opts := options.Count().SetMaxTime(2 * time.Second)
	return mm.db.Collection(d.GetC()).CountDocuments(mm.ctx, q, opts)
}

func (mm *mgoModelImpl) hasCreatedIndex(d dao.DocInter) bool {
	exist, ok := mm.indexExistMap[d.GetC()]
	if !ok {
		return false
	}
	return exist
}

func (mm *mgoModelImpl) Save(d dao.DocInter, u dao.LogUser) (primitive.ObjectID, error) {
	if u == nil {
		return primitive.NilObjectID, errors.New("logUser is nil")
	}

	collection := mm.db.Collection(d.GetC())

	d.SetCreator(u)
	result, err := collection.InsertOne(mm.ctx, d.GetDoc())
	if err != nil {
		return primitive.NilObjectID, err
	}

	if id, ok := result.InsertedID.(primitive.ObjectID); ok {
		return id, nil
	}

	return primitive.NilObjectID, errors.New("not objectID")
}

func (mm *mgoModelImpl) RemoveByID(d dao.DocInter, u dao.LogUser) (int64, error) {
	if u == nil {
		return 0, errors.New("logUser is nil")
	}
	collection := mm.db.Collection(d.GetC())
	result, err := collection.DeleteOne(mm.ctx, bson.M{"_id": d.GetID()})
	return result.DeletedCount, err
}

func (mm *mgoModelImpl) UpdateOne(d dao.DocInter, fields bson.D, u dao.LogUser) (int64, error) {
	if u == nil {
		return 0, errors.New("logUser is nil")
	}
	collection := mm.db.Collection(d.GetC())
	result, err := collection.UpdateOne(mm.ctx, bson.M{"_id": d.GetID()},
		bson.D{
			{"$set", fields},
		},
	)
	return result.ModifiedCount, err
}

func (mm *mgoModelImpl) FindByID(d dao.DocInter) error {
	return mm.FindOne(d, bson.M{"_id": d.GetID()})
}

func (mm *mgoModelImpl) FindOne(d dao.DocInter, q bson.M) error {
	collection := mm.db.Collection(d.GetC())
	return collection.FindOne(mm.ctx, q).Decode(d)
}

func (mm *mgoModelImpl) Find(d dao.DocInter, q bson.M) (interface{}, error) {
	myType := reflect.TypeOf(d)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	collection := mm.db.Collection(d.GetC())
	sortCursor, err := collection.Find(mm.ctx, q)
	if err != nil {
		return nil, err
	}
	err = sortCursor.All(mm.ctx, &slice)
	if err != nil {
		return nil, err
	}
	return slice, err
}

func (mm *mgoModelImpl) PipeFind(aggr MgoAggregate, filter bson.M) (interface{}, error) {
	myType := reflect.TypeOf(aggr)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	collection := mm.db.Collection(aggr.GetC())
	sortCursor, err := collection.Aggregate(mm.ctx, aggr.GetPipeline(filter))
	if err != nil {
		return nil, err
	}
	err = sortCursor.All(mm.ctx, &slice)
	if err != nil {
		return nil, err
	}
	return slice, err
}

func (mm *mgoModelImpl) PipeFindOne(aggr MgoAggregate, filter bson.M) error {
	fmt.Println(mm.db, aggr.GetC(), aggr.GetPipeline(filter))
	collection := mm.db.Collection(aggr.GetC())
	sortCursor, err := collection.Aggregate(mm.ctx, aggr.GetPipeline(filter))
	if err != nil {
		return err
	}
	if sortCursor.Next(mm.ctx) {
		err = sortCursor.Decode(aggr)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	return nil
}

func (mm *mgoModelImpl) PageFind(d dao.DocInter, filter bson.M, limit, page int64) (interface{}, error) {
	if limit <= 0 {
		limit = 50
	}
	if page <= 0 {
		page = 1
	}
	skip := limit * (page - 1)
	findopt := options.Find().SetSkip(skip).SetLimit(limit)
	myType := reflect.TypeOf(d)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	collection := mm.db.Collection(d.GetC())
	sortCursor, err := collection.Find(mm.ctx, filter, findopt)
	if err != nil {
		return nil, err
	}

	err = sortCursor.All(mm.ctx, &slice)
	return slice, err
}

func (mm *mgoModelImpl) PagePipeFind(aggr MgoAggregate, filter bson.M, limit, page int64) (interface{}, error) {
	if limit <= 0 {
		limit = 50
	}
	if page <= 0 {
		page = 1
	}
	skip := limit * (page - 1)
	myType := reflect.TypeOf(aggr)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()

	collection := mm.db.Collection(aggr.GetC())
	pl := append(aggr.GetPipeline(filter), bson.D{{"$skip", skip}}, bson.D{{"$limit", limit}})
	sortCursor, err := collection.Aggregate(mm.ctx, pl)
	if err != nil {
		return nil, err
	}
	err = sortCursor.All(mm.ctx, &slice)
	if err != nil {
		return nil, err
	}
	return slice, err
}

func (mm *mgoModelImpl) GetPaginationSource(d dao.DocInter, q bson.M) util.PaginationSource {
	return &mongoPaginationImpl{
		MgoDBModel: mm,
		d:          d,
		q:          q,
	}
}

type mongoPaginationImpl struct {
	MgoDBModel
	d dao.DocInter
	q bson.M
}

func (mpi *mongoPaginationImpl) Count() (int64, error) {
	return mpi.CountDocuments(mpi.d, mpi.q)
}

func (mpi *mongoPaginationImpl) Data(limit, p int64, format func(i interface{}) map[string]interface{}) ([]map[string]interface{}, error) {
	result, err := mpi.PageFind(mpi.d, mpi.q, limit, p)
	if err != nil {
		return nil, err
	}
	formatResult, l := dao.Format(result, format)
	if l == 0 {
		return nil, nil
	}
	return formatResult.([]map[string]interface{}), nil
}
