package core

import (
	"bytes"
	"context"
	"raccoon-api/core/dao"
	"raccoon-api/core/db"
	"raccoon-api/util"
	"testing"

	"raccoon-api/core"
	"raccoon-api/dao/mock"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

type M map[string]interface{}

func iniDI() {
	core.InitConfByFile("../../conf/dev/config.yml")
}

type testDoc struct {
	ID   primitive.ObjectID `bson:"_id"`
	Name string

	dao.CommonDoc `bson:"meta"`
}

func (u *testDoc) GetDoc() interface{} {
	if u.ID.IsZero() {
		u.ID = primitive.NewObjectID()
	}
	return u
}

func (u *testDoc) GetC() string {
	return "test"
}

func (u *testDoc) GetID() primitive.ObjectID {
	return u.ID
}

func (u *testDoc) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "name", Value: bsonx.String("text")}},
		},
	}
}

type testAggrDoc struct {
	ID     primitive.ObjectID `bson:"_id"`
	TestID primitive.ObjectID `bson:"testid"`

	Name string

	dao.CommonDoc `bson:"meta"`
}

func (u *testAggrDoc) GetDoc() interface{} {
	if u.ID.IsZero() {
		u.ID = primitive.NewObjectID()
	}
	return u
}

func (u *testAggrDoc) GetC() string {
	return "testaggr"
}

func (u *testAggrDoc) GetID() primitive.ObjectID {
	return u.ID
}

func (u *testAggrDoc) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "name", Value: bsonx.String("text")}},
		},
	}
}

func Test_Pagination(t *testing.T) {
	iniDI()
	di := core.GetDI()
	ctx := context.Background()
	db, err := di.GetMongoDBClt(ctx)
	assert.Nil(t, err)
	defer db.Close()
	mdbm := NewMgoModel(ctx, db.GetCoreDB(), di.GetLog("test"))

	ps := mdbm.GetPaginationSource(&testDoc{}, bson.M{"name": "jack"})
	i, err := ps.Count()
	assert.Equal(t, int64(1), i)
	assert.Nil(t, err)
	p, err := util.NewPagination(ps, 200, 1, func(i interface{}) map[string]interface{} {
		if doc, ok := i.(*testDoc); ok {
			return map[string]interface{}{
				"aaa": doc.Name,
			}
		}
		return nil
	})
	buf := new(bytes.Buffer)
	p.Output(buf)
	assert.JSONEq(t, `{"rows":[{"aaa":"jack"}],"total":1,"allPages":1,"page":1,"limit":200}`, buf.String())
}

func Test_MongoCURD(t *testing.T) {
	iniDI()
	name := "jack"
	r := &testDoc{
		Name: name,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	db, _ := di.GetMongoDBClt(ctx)

	mdbm := NewMgoModel(ctx, db.GetCoreDB(), di.GetLog("test"))
	id, err := mdbm.Save(r, u)
	assert.Nil(t, err)

	fr := &testDoc{
		ID: id,
	}
	err = mdbm.FindByID(fr)
	assert.Nil(t, err)
	assert.Equal(t, name, fr.Name)

	dr := &testDoc{
		ID: id,
	}
	delCount, err := mdbm.RemoveByID(dr, u)
	assert.Nil(t, err)
	assert.Equal(t, int64(1), delCount)
}

func Test_MongoUpdate(t *testing.T) {
	iniDI()
	name := "jack"
	newName := "peter"
	r := &testDoc{
		Name: name,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	dbclt, _ := di.GetMongoDBClt(ctx)
	defer dbclt.Close()
	mdbm := NewMgoModel(ctx, dbclt.GetCoreDB(), di.GetLog("test"))
	id, err := mdbm.Save(r, u)
	assert.Nil(t, err)

	ur := &testDoc{
		ID: id,
	}
	mcount, err := mdbm.UpdateOne(ur, bson.D{{"name", newName}}, mock.MockLogUser("mock"))
	assert.Nil(t, err)
	assert.Equal(t, int64(1), mcount)

	fr := &testDoc{
		ID: id,
	}
	err = mdbm.FindByID(fr)
	assert.Nil(t, err)
	assert.Equal(t, newName, fr.Name)
}

func Test_MongoTransCommit(t *testing.T) {
	iniDI()
	name1 := "jack"
	r1 := &testDoc{
		Name: name1,
	}
	name2 := "peter"
	r2 := &testDoc{
		Name: name2,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	dbclt, _ := di.GetMongoDBClt(ctx)
	defer dbclt.Close()
	mdbm := NewMgoModel(ctx, dbclt.GetCoreDB(), di.GetLog("test"))
	id1, err := mdbm.Save(r1, u)
	assert.Nil(t, err)

	id2, err := mdbm.Save(r2, u)
	ids := []primitive.ObjectID{id1, id2}
	assert.Nil(t, err)

	newName := "okok"
	if err := batchUpdate(dbclt, ids, newName); err != nil {
		assert.Nil(t, err)
	}

	ft1 := &testDoc{
		ID: id1,
	}

	err = mdbm.FindByID(ft1)
	assert.Nil(t, err)
	assert.Equal(t, newName, ft1.Name)

	ft2 := &testDoc{
		ID: id1,
	}

	err = mdbm.FindByID(ft2)
	assert.Nil(t, err)
	assert.Equal(t, newName, ft2.Name)
}

func Test_MongoTransAbort(t *testing.T) {
	iniDI()
	name1 := "jack"
	r1 := &testDoc{
		Name: name1,
	}
	name2 := "peter"
	r2 := &testDoc{
		Name: name2,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	dbclt, _ := di.GetMongoDBClt(ctx)
	defer dbclt.Close()
	mdbm := NewMgoModel(ctx, dbclt.GetCoreDB(), di.GetLog("test"))
	id1, err := mdbm.Save(r1, u)
	assert.Nil(t, err)

	id2, err := mdbm.Save(r2, u)
	ids := []primitive.ObjectID{id1, id2, primitive.NewObjectID()}
	assert.Nil(t, err)

	newName := "okok"
	if err := batchUpdate(dbclt, ids, newName); err != nil {
		assert.Nil(t, err)
	}

	ft1 := &testDoc{
		ID: id1,
	}

	err = mdbm.FindByID(ft1)
	assert.Nil(t, err)
	assert.Equal(t, name1, ft1.Name)

	ft2 := &testDoc{
		ID: id2,
	}

	err = mdbm.FindByID(ft2)
	assert.Nil(t, err)
	assert.Equal(t, name2, ft2.Name)
}

func batchUpdate(dbclt db.MongoDBClient, ids []primitive.ObjectID, newName string) error {
	di := core.GetDI()
	return dbclt.WithSession(func(sc mongo.SessionContext) error {
		for _, bID := range ids {
			r := &testDoc{
				ID: bID,
			}
			mdbm := NewMgoModel(sc, dbclt.GetCoreDB(), di.GetLog("test"))
			count, err := mdbm.UpdateOne(r, bson.D{{"name", newName}}, mock.MockLogUser("mock"))

			if err != nil || count == 0 {
				return dbclt.AbortTransaction(sc)
			}
		}
		return dbclt.CommitTransaction(sc)
	})
}

func Test_MongoManyToOnePipe(t *testing.T) {
	iniDI()
	name := "jack"
	r := &testDoc{
		Name: name,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	db, _ := di.GetMongoDBClt(ctx)

	mdbm := NewMgoModel(ctx, db.GetCoreDB(), di.GetLog("test"))
	id, err := mdbm.Save(r, u)
	assert.Nil(t, err)

	ta := &testAggrDoc{
		Name:   "aggr",
		TestID: id,
	}
	id, err = mdbm.Save(ta, u)
	assert.Nil(t, err)
	taht := &testAggrHasTest{}
	result, err := mdbm.PipeFind(taht, bson.M{"_id": id})
	tahtlist := result.([]*testAggrHasTest)
	for _, tl := range tahtlist {
		assert.Equal(t, name, tl.TestDoc.Name)
	}
	assert.Nil(t, err)
}

type testAggrHasTest struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	TestDoc testDoc            `bson:"testDoc,omitempty"`
	Name    string             `bson:"name,omitempty"`
}

func (u *testAggrHasTest) GetC() string {
	return "testaggr"
}

func (t *testAggrHasTest) GetPipeline(q bson.M) mongo.Pipeline {
	matchStage := bson.D{{"$match", q}}
	lookupStage := bson.D{{"$lookup", bson.D{{"from", "test"}, {"localField", "testid"}, {"foreignField", "_id"}, {"as", "testDoc"}}}}
	unwindStage := bson.D{{"$unwind", bson.D{{"path", "$testDoc"}, {"preserveNullAndEmptyArrays", false}}}}
	return mongo.Pipeline{matchStage, lookupStage, unwindStage}
}

func Test_MongoOneToManyPipe(t *testing.T) {
	iniDI()
	name := "jack"
	r := &testDoc{
		Name: name,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	db, _ := di.GetMongoDBClt(ctx)

	mdbm := NewMgoModel(ctx, db.GetCoreDB(), di.GetLog("test"))
	tid, err := mdbm.Save(r, u)
	assert.Nil(t, err)

	ta := &testAggrDoc{
		Name:   "aggr1",
		TestID: tid,
	}
	_, err = mdbm.Save(ta, u)
	assert.Nil(t, err)

	ta2 := &testAggrDoc{
		Name:   "aggr2",
		TestID: tid,
	}
	_, err = mdbm.Save(ta2, u)
	assert.Nil(t, err)

	taht := &testHasTestAggr{}
	result, err := mdbm.PipeFind(taht, bson.M{"_id": tid})
	tahtlist := result.([]*testHasTestAggr)
	for _, tl := range tahtlist {
		assert.Equal(t, 2, len(tl.TestAggrList))
		assert.Equal(t, "aggr2", tl.TestAggrList[1].Name)
		assert.Equal(t, "aggr1", tl.TestAggrList[0].Name)
	}
	assert.Nil(t, err)
}

type testHasTestAggr struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	TestAggrList []*testAggrDoc     `bson:"testAggrList,omitempty"`
	Name         string             `bson:"name,omitempty"`
}

func (u *testHasTestAggr) GetC() string {
	return "test"
}

func (u *testHasTestAggr) GetPipeline(q bson.M) mongo.Pipeline {
	matchStage := bson.D{{"$match", q}}
	lookupStage := bson.D{{"$lookup", bson.D{{"from", "testaggr"}, {"localField", "_id"}, {"foreignField", "testid"}, {"as", "testAggrList"}}}}
	return mongo.Pipeline{matchStage, lookupStage}
}

func Test_MongoPipeFindOne(t *testing.T) {
	iniDI()
	name := "jack"
	r := &testDoc{
		Name: name,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	db, _ := di.GetMongoDBClt(ctx)

	mdbm := NewMgoModel(ctx, db.GetCoreDB(), di.GetLog("test"))
	tid, err := mdbm.Save(r, u)
	assert.Nil(t, err)

	ta := &testAggrDoc{
		Name:   "aggr1",
		TestID: tid,
	}
	_, err = mdbm.Save(ta, u)
	assert.Nil(t, err)

	ta2 := &testAggrDoc{
		Name:   "aggr2",
		TestID: tid,
	}
	_, err = mdbm.Save(ta2, u)
	assert.Nil(t, err)

	taht := &testHasTestAggr{}
	err = mdbm.PipeFindOne(taht, bson.M{"_id": tid})
	assert.Nil(t, err)
	assert.Equal(t, tid, taht.ID)
	assert.Equal(t, 2, len(taht.TestAggrList))
	assert.Equal(t, "aggr2", taht.TestAggrList[1].Name)
	assert.Equal(t, "aggr1", taht.TestAggrList[0].Name)
	assert.Nil(t, err)
}

func Test_MongoPagePipeFind(t *testing.T) {
	iniDI()
	name := "jack"
	r := &testDoc{
		Name: name,
	}
	u := mock.MockLogUser("mock")
	di := core.GetDI()
	ctx := context.Background()
	db, _ := di.GetMongoDBClt(ctx)

	mdbm := NewMgoModel(ctx, db.GetCoreDB(), di.GetLog("test"))
	tid, err := mdbm.Save(r, u)
	assert.Nil(t, err)

	ta := &testAggrDoc{
		Name:   "aggr1",
		TestID: tid,
	}
	_, err = mdbm.Save(ta, u)
	assert.Nil(t, err)

	ta2 := &testAggrDoc{
		Name:   "aggr2",
		TestID: tid,
	}
	_, err = mdbm.Save(ta2, u)
	assert.Nil(t, err)

	taht := &testAggrHasTest{}
	result, err := mdbm.PagePipeFind(taht, bson.M{"testid": tid}, 1, 1)
	tahtlist := result.([]*testAggrHasTest)
	assert.Equal(t, 1, len(tahtlist))
	assert.Nil(t, err)
}
