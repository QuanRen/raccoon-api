package core

import (
	"context"
	"raccoon-api/core"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Ping(t *testing.T) {
	iniDI()
	di := core.GetDI()
	ctx := context.Background()

	sm := NewSearchModel(ctx, di.GetSearch(), di.GetLog("test"))
	statusCode := sm.Ping(`https://ad348ea308674762b9f3565f472150e8.us-east-1.aws.found.io:9243/`)
	assert.Equal(t, 200, statusCode)
}
