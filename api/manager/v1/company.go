package v1

import (
	"encoding/json"
	"fmt"
	"net/http"

	"raccoon-api/api/input"
	"raccoon-api/core/api"
	"raccoon-api/core/db"
	"raccoon-api/model/core"
)

type CompanyAPI string

func (a CompanyAPI) GetName() string {
	return string(a)
}

func (a CompanyAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		&api.APIHandler{Path: "/v1/company", Next: a.createEndpoint, Method: "POST", Auth: true},
	}
}

func (a CompanyAPI) Init() {

}

func (a CompanyAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	fmt.Println("create")
	cb := input.CreateCompany{}
	err := json.NewDecoder(req.Body).Decode(&cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	mm := core.NewMgoModelByReq(req, db.UserDB)
	id, err := mm.Save(cb.ToDoc(), ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	fmt.Println(id, err)
	w.Write([]byte("ok"))
}
