package v1

import (
	"encoding/json"
	"fmt"
	"net/http"
	"raccoon-api/api/input"
	"raccoon-api/core"
	"raccoon-api/core/api"
	"raccoon-api/core/db"
	"raccoon-api/core/log"
	"raccoon-api/model"
	cmodel "raccoon-api/model/core"
	"raccoon-api/util"
)

type AuthAPI string

func (a AuthAPI) GetName() string {
	return string(a)
}

func (a AuthAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		&api.APIHandler{Path: "/v1/login", Next: a.loginEndpoint, Method: "POST", Auth: false},
		&api.APIHandler{Path: "/v1/token/_check", Next: a.checkTokenEndpoint, Method: "GET", Auth: true},
	}
}

func (a AuthAPI) Init() {

}

func (a *AuthAPI) checkTokenEndpoint(w http.ResponseWriter, req *http.Request) {
	u := input.GetStoreUser(req)
	fmt.Println(u)
}

func (a *AuthAPI) loginEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := input.LoginWithCompany{}
	err := json.NewDecoder(req.Body).Decode(&cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	cb.ClientInfo = util.GetClientInfo(req)
	ctx := req.Context()
	l := log.GetCtxLog(req)
	di := core.GetDI()
	dbclt, err := di.GetMongoDBClt(ctx, db.GetUserDBName(cb.Vat))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Get db clt err: " + err.Error()))
		return
	}
	dbmodel := cmodel.NewMgoModel(ctx, dbclt.GetUserDB(), l)
	um := model.NewAuth(dbmodel, l)
	u, err := um.MgrSignIn(cb)
	if err != nil {
		if apierr, ok := err.(api.ApiError); ok {
			w.WriteHeader(apierr.GetStatus())
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}

	token, err := di.GetJWTConf().GetToken(u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("create token err: " + err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token":      token,
		"permission": u.GetPerm(),
		"state":      "normal",
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
