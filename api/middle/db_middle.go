package middle

import (
	"net/http"
	"runtime"

	"raccoon-api/api/input"
	"raccoon-api/core"
	"raccoon-api/core/db"
	"raccoon-api/core/log"
	"raccoon-api/util"

	"github.com/google/uuid"
)

type DBMiddle string

func (am DBMiddle) GetName() string {
	return string(am)
}

func (am DBMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			reqID := ctx.Value(input.CtxUserInfoKey)
			var dbclt db.MongoDBClient
			var err error
			uuid := uuid.New().String()
			di := core.GetDI()
			l := di.GetLog(uuid)
			ret, ok := reqID.(input.ReqUser)
			if ok {
				dbclt, err = di.GetMongoDBClt(r.Context(), db.GetUserDBName(ret.GetVat()))
			} else {
				dbclt, err = di.GetMongoDBClt(r.Context(), "")
			}
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}

			r = util.SetCtxKeyVal(r, db.CtxMongoKey, dbclt)
			r = util.SetCtxKeyVal(r, log.CtxLogKey, l)
			f(w, r)
			dbclt.Close()
			runtime.GC()
		}
	}
}
