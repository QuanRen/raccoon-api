package middle

import (
	"net/http"

	"raccoon-api/api/input"
	"raccoon-api/util"
)

type FakeAuthMiddle string

func (am FakeAuthMiddle) GetName() string {
	return string(am)
}

func (am FakeAuthMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			r.Header.Set("isLogin", "true")

			r = util.SetCtxKeyVal(r, input.CtxUserInfoKey, input.NewReqStoreUser(
				"25077808", "testing@gmail.com", "testing", "test", "store",
			))
		}
	}
}
