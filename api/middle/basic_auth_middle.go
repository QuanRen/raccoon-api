package middle

import (
	"fmt"
	"net/http"
	"raccoon-api/api/input"
	"raccoon-api/core"
	"raccoon-api/util"

	jwt "github.com/dgrijalva/jwt-go"

	"github.com/gorilla/mux"
)

type AuthMiddle string

const (
	authValue       = uint8(1 << iota)
	allowMultiValue = uint8(1 << iota)
	developValue    = uint8(1 << iota)

	AuthTokenKey = "Auth-Token"
)

var (
	authMap  map[string]uint8    = make(map[string]uint8)
	groupMap map[string][]string = make(map[string][]string)
)

func isDevelop(path string, method string) bool {
	key := fmt.Sprintf("%s:%s", path, method)
	value, ok := authMap[key]

	if ok {
		return (value & developValue) > 0
	}
	return false
}

func isAuth(path string, method string) bool {
	key := fmt.Sprintf("%s:%s", path, method)
	value, ok := authMap[key]

	if ok {
		return (value & authValue) > 0
	}
	return false
}

func hasPerm(path string, method string, perm string) bool {
	key := fmt.Sprintf("%s:%s", path, method)
	value, ok := groupMap[key]
	if len(value) == 0 {
		return true
	}
	if ok && util.IsStrInList(perm, value...) {
		return true
	}
	return false
}

func (am AuthMiddle) GetName() string {
	return string(am)
}

func isDevelopToken(t *jwt.Token) bool {
	kid, ok := t.Header["kid"]
	if ok {
		return kid == "dev"
	}
	return false
}

func isUserToken(t *jwt.Token) bool {
	kid, ok := t.Header["kid"]
	di := core.GetDI()
	if ok {
		return kid == di.GetJWTConf().Header.Kid
	}
	return false
}

func (bam AuthMiddle) AddAuthPath(path string, method string, auth bool, group []string) {
	path = getPathKey(path, method)
	value := uint8(0)
	if auth {
		value = value | authValue
	}
	authMap[path] = uint8(value)
	groupMap[path] = group
}

func getPathKey(path, method string) string {
	return fmt.Sprintf("%s:%s", path, method)
}

func (am AuthMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			// ... pre handler functionality
			path, err := mux.CurrentRoute(r).GetPathTemplate()
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(err.Error()))
				return
			}
			di := core.GetDI()
			auth := isAuth(path, r.Method)
			if auth {
				authToken := r.Header.Get(AuthTokenKey)
				if authToken == "" {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("miss token"))
					return
				}
				jwtToken, err := di.GetJWTConf().Parse(authToken)
				if err != nil {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte(err.Error()))
					return
				}

				if !isUserToken(jwtToken) {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				mapClaims := jwtToken.Claims.(jwt.MapClaims)
				permission, ok := mapClaims["per"].(string)
				if hasPerm := hasPerm(path, r.Method, permission); ok && !hasPerm {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("permission error"))
					return
				}
				r.Header.Set("isLogin", "true")
				if store, ok := mapClaims["sto"]; ok {
					ru := input.NewReqStoreUser(
						mapClaims["vat"].(string),
						mapClaims["sub"].(string),
						mapClaims["nam"].(string),
						permission,
						store.(string),
					)
					r = util.SetCtxKeyVal(r, input.CtxUserInfoKey, ru)
				} else {
					ru := input.NewReqUser(
						mapClaims["vat"].(string),
						mapClaims["sub"].(string),
						mapClaims["nam"].(string),
						permission,
					)
					r = util.SetCtxKeyVal(r, input.CtxUserInfoKey, ru)
				}
			}
			develop := isDevelop(path, r.Method)
			if !auth && develop {
				authToken := r.Header.Get(AuthTokenKey)
				if authToken == "" {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("miss token"))
					return
				}

				jwtToken, err := di.GetJWTConf().Parse(authToken)
				if err != nil {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte(err.Error()))
					return
				}

				if !isDevelopToken(jwtToken) {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				mapClaims := jwtToken.Claims.(jwt.MapClaims)
				if sys, ok := mapClaims["sys"].(string); ok {
					if !util.IsStrInList(sys, "racc") {
						w.WriteHeader(http.StatusUnauthorized)
						return
					}
				}
			}
			f(w, r)
		}
	}
}
