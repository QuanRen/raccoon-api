package middle

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type DebugMiddle string

func (lm DebugMiddle) GetName() string {
	return string(lm)
}

func (lm DebugMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			log.SetPrefix("DebugMid ")
			log.Println("-------Debug Request-------")

			path, _ := mux.CurrentRoute(r).GetPathTemplate()
			path = fmt.Sprintf("%s,%s?%s", r.Method, r.URL.Path, r.URL.RawQuery)
			log.Println("path: " + path)
			log.Println("remote address: " + r.RemoteAddr)
			log.Println("User Agent: " + r.UserAgent())
			header, _ := json.Marshal(r.Header)
			log.Println("header: " + string(header))
			b, err := ioutil.ReadAll(r.Body)
			r.Body.Close()
			r.Body = ioutil.NopCloser(bytes.NewBuffer(b))
			if err != nil {
				log.Printf("Error reading body: %v", err)
				http.Error(w, "can't read body", http.StatusBadRequest)
				return
			}
			log.Println("body: " + string(b))

			start := time.Now()

			f(w, r)
			delta := time.Now().Sub(start)
			if delta.Seconds() > 3 {
				log.Println("too slow")
			}
			log.Println("-------End Debug Request-------")
		}
	}
}
