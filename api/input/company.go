package input

import (
	"errors"

	"raccoon-api/core/dao"
	"raccoon-api/dao/doc"
	"raccoon-api/util"
)

type CreateCompany struct {
	Typ       string `json:"type"`
	VATnumber string `json:"vatNumber"`
	Name      string
	// AddressInfo struct {
	// 	PostalCode string `json:"postalCode"`
	// 	Country    string `json:"country"`
	// 	City       string `json:"city"`
	// 	Address    string `json:"address"`
	// } `json:"addressInfo"`
	// Fax           string
	// Owner         string
	// ContactPerson struct {
	// 	Name    string
	// 	Contact struct {
	// 		CtType string `json:"ctType"` // enum: ["office"]
	// 		Value  string
	// 	}
	// } `json:"contactPerson"`
}

func (cc *CreateCompany) Validate() error {
	if !util.IsStrInList(cc.Typ, doc.CompanyTypEnterprise) {
		return errors.New("invalid company type")
	}

	if util.IsVATnumber(cc.VATnumber) {
		return errors.New("invalid vatNumber")
	}
	return nil
}

func (cc *CreateCompany) ToDoc() dao.DocInter {
	return &doc.Company{
		Typ:       cc.Typ,
		Name:      cc.Name,
		VATnumber: cc.VATnumber,
		IsFake:    false,
		CommonDoc: dao.CommonDoc{},
	}

}
