package input

import (
	"errors"
	"net/http"
	"raccoon-api/core/dao"
	"raccoon-api/util"
)

const (
	CtxUserInfoKey = util.CtxKey("userInfo")
)

func GetStoreUser(req *http.Request) ReqStoreUser {
	ctx := req.Context()
	reqID := ctx.Value(CtxUserInfoKey)
	if ret, ok := reqID.(ReqStoreUser); ok {
		return ret
	}
	return nil
}

func NewReqStoreUser(vat, acc, name, perm, store string) ReqStoreUser {
	return reqStoreUserImpl{
		ReqUser: NewReqUser(vat, acc, name, perm),
		store:   store,
	}
}

type ReqStoreUser interface {
	ReqUser
	GetStore() string
}

type reqStoreUserImpl struct {
	ReqUser
	store string
}

func (re reqStoreUserImpl) GetStore() string {
	return re.store
}

type ReqUser interface {
	dao.LogUser
	GetPerm() string
	GetVat() string
}

type reqUserImpl struct {
	Account string
	Name    string
	Perm    string
	Vat     string
}

func (ru reqUserImpl) GetName() string {
	return ru.Name
}

func (ru reqUserImpl) GetAccount() string {
	return ru.Account
}

func (ru reqUserImpl) GetPerm() string {
	return ru.Perm
}

func (ru reqUserImpl) GetVat() string {
	return ru.Vat
}

func NewReqUser(vat, acc, name, perm string) ReqUser {
	return reqUserImpl{
		Account: acc,
		Name:    name,
		Perm:    perm,
		Vat:     vat,
	}
}

func GetUserInfo(req *http.Request) ReqUser {
	ctx := req.Context()
	reqID := ctx.Value(CtxUserInfoKey)
	if ret, ok := reqID.(ReqUser); ok {
		return ret
	}
	return nil
}

type Register struct {
	Account string
	Pwd     string
	Perm    string
	Key     string
}

func (qb *Register) Validate() error {
	if qb.Account == "" {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss pwd")
	}
	if qb.Perm == "" {
		return errors.New("miss perm")
	}
	if qb.Key == "" {
		return errors.New("miss key")
	}
	return nil
}

type ChangePwd struct {
	New, Old string
}

func (qb *ChangePwd) Validate() error {
	if qb.New == "" {
		return errors.New("missing new pwd")
	}
	if qb.Old == "" {
		return errors.New("missing old pwd")
	}
	return nil
}
