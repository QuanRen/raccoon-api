package input

import (
	"errors"
	"raccoon-api/util"

	"github.com/asaskevich/govalidator"
)

type LoginWithCompany struct {
	BasicLogin
	Vat string `json:"company"`
}

func (qb *LoginWithCompany) Validate() error {
	if err := qb.BasicLogin.Validate(); err != nil {
		return err
	}
	if len(qb.Vat) != 8 {
		return errors.New("vat len error")
	}
	if ok := util.IsVATnumber(qb.Vat); !ok {
		return errors.New("invalid vat")
	}
	return nil
}

type BasicLogin struct {
	Account    string
	Pwd        string
	ClientInfo map[string]interface{}
}

func (qb *BasicLogin) Validate() error {
	if !govalidator.IsEmail(qb.Account) {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}
	return nil
}
