package doc

import (
	"errors"
	"time"

	"raccoon-api/core/dao"
	"raccoon-api/util"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

const (
	UserPermOwn  = "own"
	UserPermPro  = "pro"
	UserPermUser = "user"

	UserStateNew    = "new"
	UserStateNormal = "normal"

	ContactTypeMobile = "mobile"
	ContactTypeHome   = "home"
	ContactTypeOffice = "office"

	userC      = "user"
	userStoreC = "userStore"
)

type ContactInfo struct {
	Contacts []contactRecord // 聯絡電話
}

func (ci ContactInfo) GetPhone() string {
	if len(ci.Contacts) == 0 {
		return ""
	}
	return ci.Contacts[0].Value
}

func (ci *ContactInfo) AddContact(typ string, val string) {
	if !util.IsStrInList(typ, ContactTypeMobile, ContactTypeHome, ContactTypeOffice) {
		return
	}
	ci.Contacts = append(ci.Contacts, contactRecord{
		ID:    primitive.NewObjectID().Hex(),
		Typ:   typ,
		Value: val,
	})
}

type contactRecord struct {
	ID    string
	Typ   string
	Value string
}

type LoginInfo struct {
	Times    int       // 登入次數
	LastTime time.Time // 最近一次登入時間
	Records  []loginRecord
}

func (li *LoginInfo) AddLoginRecord(data map[string]interface{}) {
	if len(li.Records) >= 5 {
		li.Records = li.Records[1:]
	}
	li.Times++
	li.LastTime = time.Now()
	li.Records = append(li.Records, loginRecord{
		Time: li.LastTime,
		Info: data,
	})
}

type loginRecord struct {
	Time time.Time
	Info map[string]interface{}
}

// 使用者資料
type User struct {
	ID            primitive.ObjectID `bson:"_id"`
	Email         string
	EmailVerified bool `bson:"-"`
	PhoneNumber   string
	Pwd           string
	DisplayName   string
	Disabled      bool

	ContactInfo   `bson:"contactinfo"`
	LoginInfo     `bson:"logininfo"`
	dao.CommonDoc `bson:"meta"`
}

func (u *User) GetState() string {
	if u.Times == 1 {
		return UserStateNew
	}
	return UserStateNormal
}

func (u *User) GetC() string {
	return userC
}
func (u *User) GetDoc() interface{} {
	u.ID = primitive.NewObjectID()
	return u
}
func (u *User) GetID() primitive.ObjectID {
	return u.ID
}
func (u *User) GetUpdateField() bson.M {
	return bson.M{}
}

func (u *User) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bsonx.Doc{
				{Key: "email", Value: bsonx.String("text")},
				{Key: "pwd", Value: bsonx.String("text")},
			},
		},
		{
			Keys: bsonx.Doc{{Key: "email", Value: bsonx.String("text")}},
		},
	}
}

func (u *User) GetAccount() string {
	return u.Email
}

func (u *User) GetName() string {
	return u.DisplayName
}

func (u *User) GetJwtData() map[string]interface{} {
	return map[string]interface{}{
		"sub": u.ID.Hex(),
		"acc": u.Email,
		"nam": u.DisplayName,
		// "per": ucp.Permission,
		// "cat": ucp.CompanyID.Hex(),
	}
}

type SimpleUser struct {
	ID   primitive.ObjectID
	Name string
}

// 公司使用者
type CompanyUser struct {
	User `bson:",inline"`
	Vat  string
}

// 使用者店家權限
type UserStorePerm struct {
	ID         primitive.ObjectID `bson:"_id"`
	UserID     primitive.ObjectID
	StoreID    primitive.ObjectID
	Permission string
	Note       string

	dao.CommonDoc `bson:"meta"`
}

func (u *UserStorePerm) GetC() string {
	return userStoreC
}

func (u *UserStorePerm) GetDoc() interface{} {
	u.ID = primitive.NewObjectID()
	return u
}

func (u *UserStorePerm) GetID() primitive.ObjectID {
	return u.ID
}

func (u *UserStorePerm) GetUpdateField() bson.M {
	return bson.M{
		"permission": u.Permission,
		"note":       u.Note,
	}
}

// 取得使用者及店家清單
type UserHasStoreAggr struct {
	CompanyUser   `bson:",inline"`
	StoreAggrList []*UserStorePerm
}

func (u *UserHasStoreAggr) GetPipeline(q bson.M) mongo.Pipeline {
	matchStage := bson.D{{"$match", q}}
	lookupStage := bson.D{{"$lookup", bson.D{{"from", userStoreC}, {"localField", "_id"}, {"foreignField", "userid"}, {"as", "storeaggrlist"}}}}
	return mongo.Pipeline{matchStage, lookupStage}
}

func (u *UserHasStoreAggr) GetJwtData() (map[string]interface{}, error) {
	if len(u.StoreAggrList) != 1 {
		return nil, errors.New("user store setting problem: " + u.ID.Hex())
	}
	if u.Vat == "" {
		return nil, errors.New("invalid vat")
	}
	usp := u.StoreAggrList[0]

	return map[string]interface{}{
		"sub": u.ID.Hex(),
		"acc": u.Email,
		"nam": u.DisplayName,
		"per": usp.Permission,
		"vat": u.Vat,
		"sto": usp.ID.Hex(),
	}, nil
}

func (u *UserHasStoreAggr) GetPerm() string {
	if len(u.StoreAggrList) != 1 {
		return "none"
	}
	usp := u.StoreAggrList[0]
	return usp.Permission
}

func (u *UserHasStoreAggr) GetVat() string {
	return u.Vat
}
