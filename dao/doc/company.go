package doc

import (
	"encoding/binary"
	"errors"
	"strconv"

	"raccoon-api/core/dao"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

const (
	companyC = "company"

	CompanyTypEnterprise = "enterprise"
)

type AddressInfo struct {
	PostalCode string
	Country    string
	City       string
	Address    string
}

type ContactPersonInfo struct {
	Name string
	ContactInfo
}

type Company struct {
	ID        primitive.ObjectID `bson:"_id"`
	Typ       string
	Name      string
	VATnumber string // 統一編號
	IsFake    bool   // 是否假的資料
	OwnerName string
	AddressInfo
	ContactPersonInfo

	dao.CommonDoc `bson:"meta"`
}

func GetCompanyId(unitCode string, isFake bool) (primitive.ObjectID, error) {
	var b [12]byte
	i, err := strconv.ParseUint(unitCode, 10, 64)
	if err != nil {
		return primitive.NilObjectID, errors.New("invalid unitCode")
	}
	if isFake {
		i = (1 << 30) + i
	}
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, i)
	for i = 0; i < 8; i++ {
		b[i] = bs[i]
	}
	return b, nil
}

func (d *Company) newID() primitive.ObjectID {
	id, err := GetCompanyId(d.VATnumber, d.IsFake)
	if err != nil {
		panic(err)
	}
	return id
}

func (u *Company) GetDoc() interface{} {
	if u.ID.IsZero() {
		u.ID = u.newID()
	}
	return u
}

func (u *Company) GetC() string {
	return companyC
}

func (u *Company) GetID() primitive.ObjectID {
	return u.ID
}

func (u *Company) GetUpdateField() bson.M {
	return bson.M{}
}

func (u *Company) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "typ", Value: bsonx.String("text")}},
		},
	}
}
