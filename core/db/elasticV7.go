package db

import (
	elastic "github.com/olivere/elastic/v7"
)

type Searchv7 struct {
	Url  string `yaml:"url"`
	User string
	Pwd  string

	client *elastic.Client
}

func (s *Searchv7) GetClient() *elastic.Client {
	if s.client != nil {
		return s.client
	}

	c := s.connect()
	s.client = c

	return c
}

func (s *Searchv7) connect() *elastic.Client {
	myclient, err := elastic.NewClient(
		elastic.SetURL(s.Url),
		elastic.SetSniff(false),
		elastic.SetBasicAuth(s.User, s.Pwd))
	if err != nil {
		panic(err)
	}
	return myclient
}
