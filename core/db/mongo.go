package db

import (
	"context"
	"fmt"
	"net/http"
	"raccoon-api/util"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const (
	CtxMongoKey = util.CtxKey("ctxMongoKey")
	HeaderDBKey = "raccMongoDB"
)

func GetUserDBName(vat string) string {
	return util.MD5(vat)
}

func GetCtxMgoDabase(req *http.Request, source string) *mongo.Database {
	ctx := req.Context()
	cltInter := ctx.Value(CtxMongoKey)

	if dbclt, ok := cltInter.(MongoDBClient); ok {
		switch source {
		case CoreDB:
			return dbclt.GetCoreDB()
		default:
			return dbclt.GetUserDB()
		}
	}
	return nil
}

type MongoClient interface {
	getDB(db string) *mongo.Database
	Close()
	Ping() error
	GetDBList() ([]string, error)
	GetDBClt(userDB string) MongoDBClient
}

type MongoConf struct {
	Host      string `yaml:"host"`
	User      string `yaml:"user"`
	Pass      string `yaml:"pass"`
	DefaultDB string `yaml:"defaul"`

	lock     sync.Mutex
	connPool map[string]*mongo.Client
}

func (mc MongoConf) NewMongoDBClient(ctx context.Context, userDB string) (MongoDBClient, error) {
	uri := fmt.Sprintf("mongodb+srv://%s:%s@%s/<dbname>?retryWrites=true&w=majority", mc.User, mc.Pass, mc.Host)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}
	session, err := client.StartSession()
	if err != nil {
		return nil, err
	}
	if userDB == "" {
		userDB = mc.DefaultDB
	}
	return &mgoClientImpl{
		clt:       client,
		ctx:       ctx,
		cancel:    cancel,
		dbPool:    make(map[string]*mongo.Database),
		session:   session,
		defaultDB: mc.DefaultDB,
		userDB:    userDB,
	}, nil
}

type mgoClientImpl struct {
	clt     *mongo.Client
	ctx     context.Context
	cancel  context.CancelFunc
	dbPool  map[string]*mongo.Database
	session mongo.Session

	defaultDB string
	userDB    string
}

func (m *mgoClientImpl) WithSession(f func(sc mongo.SessionContext) error) error {
	if err := m.session.StartTransaction(); err != nil {
		return err
	}
	return mongo.WithSession(m.ctx, m.session, f)
}

func (m *mgoClientImpl) GetDBList() ([]string, error) {
	return m.clt.ListDatabaseNames(m.ctx, bson.M{})
}

func (m *mgoClientImpl) getDB(db string) *mongo.Database {
	if db == "" {
		db = m.defaultDB
	}
	if dbclt, ok := m.dbPool[db]; ok {
		return dbclt
	}
	dbclt := m.clt.Database(db)
	m.dbPool[db] = dbclt
	return dbclt
}

func (m *mgoClientImpl) Close() {
	if m == nil {
		return
	}
	if m.session != nil {
		m.session.EndSession(m.ctx)
	}
	m.clt.Disconnect(m.ctx)
	m.cancel()
}

func (m *mgoClientImpl) Ping() error {
	return m.clt.Ping(m.ctx, readpref.Primary())
}

func (m *mgoClientImpl) GetCoreDB() *mongo.Database {
	return m.getDB(m.defaultDB)
}

func (m *mgoClientImpl) GetUserDB() *mongo.Database {
	return m.getDB(m.userDB)
}

// func (m *mgoClientImpl) GetDBClt(userDB string) MongoDBClient {
// 	m.userDB = userDB
// 	return m
// }

func (m *mgoClientImpl) AbortTransaction(sc mongo.SessionContext) error {
	return m.session.AbortTransaction(sc)
}
func (m *mgoClientImpl) CommitTransaction(sc mongo.SessionContext) error {
	return m.session.CommitTransaction(sc)
}

const (
	CoreDB = "core"
	UserDB = "user"
)

type MongoDBClient interface {
	GetCoreDB() *mongo.Database
	GetUserDB() *mongo.Database
	WithSession(f func(sc mongo.SessionContext) error) error
	AbortTransaction(sc mongo.SessionContext) error
	CommitTransaction(sc mongo.SessionContext) error
	Close()
}
