package core

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"raccoon-api/core/db"
	"time"

	"raccoon-api/core/api"
	"raccoon-api/core/log"
	"raccoon-api/util"

	"github.com/olivere/elastic/v7"
	yaml "gopkg.in/yaml.v2"
)

type di struct {
	Mongo  *db.MongoConf `yaml:"mongo,omitempty"`
	Search *db.Searchv7  `yaml:"search,omitempty"`

	LogConf *log.LoggerConf `yaml:"log,omitempty"`
	APIConf *api.APIConf    `yaml:"api,omitempty"`
	JWTConf *api.JwtConf    `yaml:"jwtConf"`
}

func (d *di) GetMongoDBClt(ctx context.Context, userDB string) (db.MongoDBClient, error) {
	if d.Mongo == nil {
		panic("mongo not set")
	}
	return d.Mongo.NewMongoDBClient(ctx, userDB)
}

func (d *di) GetSearch() *elastic.Client {
	if d == nil || d.Search == nil {
		panic("not set search")
	}
	return d.Search.GetClient()
}

func (d *di) GetLog(key string) log.Logger {
	if d.LogConf == nil {
		panic("log not set")
	}
	return d.LogConf.NewLogger(key)
}

func (d *di) GetAPIConf() *api.APIConf {
	if d.APIConf == nil {
		panic("apiConf not set")
	}
	return d.APIConf
}

func (d *di) GetJWTConf() *api.JwtConf {
	if d.JWTConf == nil {
		panic("apiConf not set")
	}
	return d.JWTConf
}

var mydi *di

func GetDI() *di {
	if mydi == nil {
		panic("not init di")
	}
	return mydi
}

func InitConfByFile(f string) {
	yamlFile, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}
	mydi = &di{}
	err = yaml.Unmarshal(yamlFile, mydi)
	if err != nil {
		panic(err)
	}
	util.InitValidator()
}

var _env = ""

// 初始化設定檔，讀YAML檔
func IniConfByEnv(cpath, env string) {
	const confFileTpl = "%s/%s/config.yml"
	_env = env
	InitConfByFile(fmt.Sprintf(confFileTpl, cpath, env))
}

func IsTest() bool {
	return _env == "test"
}

func SystemPwd() string {
	md5pwd := os.Getenv("ENCR_WORD")
	return md5pwd
}

func (d *di) Close(key string) {

}

type modbusConf struct {
	ReadDuration  string `yaml:"rd"`
	WriteDuration string `yaml:"wd"`
}

func (mc modbusConf) GetReadDuration() time.Duration {
	d, err := time.ParseDuration(mc.ReadDuration)
	if err != nil {
		panic(err)
	}
	return d
}

func (mc modbusConf) GetWriteDuration() time.Duration {
	d, err := time.ParseDuration(mc.WriteDuration)
	if err != nil {
		panic(err)
	}
	return d
}
