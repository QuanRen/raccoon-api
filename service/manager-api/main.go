package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"runtime/pprof"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"

	"raccoon-api/core"

	v1 "raccoon-api/api/manager/v1"
	"raccoon-api/api/middle"
	"raccoon-api/core/api"
	"raccoon-api/core/api/mid"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
var memprofile = flag.String("memprofile", "", "write memory profile to `file`")
var confpath = flag.String("confpath", "./", "write memory profile to `file`")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	err := godotenv.Load(*confpath + ".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	env := os.Getenv("ENV")
	if env == "" {
		env = "dev"
		log.Printf("Defaulting to ENV %s", env)
	}

	core.IniConfByEnv(*confpath, env)

	di := core.GetDI()

	runAPI(di.GetAPIConf())

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
		f.Close()
	}
}

func runAPI(apiConf *api.APIConf) {
	router := mux.NewRouter()
	authMiddle := middle.AuthMiddle("auth")
	apiConf.InitAPI(
		router,
		[]mid.Middle{
			middle.DebugMiddle("debug"),
			middle.FakeAuthMiddle("fakeAuth"),
			authMiddle,
			middle.DBMiddle("db"),
		},
		authMiddle,
		v1.CompanyAPI("companyV1"),
		v1.AuthAPI("authV1"),
	)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", apiConf.Port), router))
}
