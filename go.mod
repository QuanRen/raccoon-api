module raccoon-api

go 1.12

require (
	github.com/94peter/pica v0.0.0-20190702091706-f6f93990fc89
	github.com/asaskevich/govalidator v0.0.0-20200819183940-29e1ff8eb0bb
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/olivere/elastic/v7 v7.0.21
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.5.1
	go.mongodb.org/mongo-driver v1.4.1
	gopkg.in/tomb.v2 v2.0.0-20161208151619-d5d1b5820637 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
