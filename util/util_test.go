package util

import (
	"encoding/binary"
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_idtest(t *testing.T) {
	// //
	ok := IsIdNumber("A123456780")
	fmt.Println(ok)
	assert.True(t, false)
}

func Test_SliceSplit(t *testing.T) {
	a := []int{1, 2, 3, 4, 5, 6}
	fmt.Println(a)
	start := 0
	l := 3
	fmt.Println(a[start : start+l])
	assert.True(t, false)
}

func Test_Byte(t *testing.T) {
	a := uint16(1)
	fmt.Println(uint32(a) << 16)
	assert.True(t, false)
}

func Test_ByteToFloat32(t *testing.T) {
	a := uint16(18000)
	b := uint16(8192)
	ba := make([]byte, 2)
	bb := make([]byte, 2)
	binary.BigEndian.PutUint16(ba, a)
	binary.BigEndian.PutUint16(bb, b)
	fmt.Println(ba, bb)

	bf := []byte{ba[0], ba[1], bb[0], bb[1]}

	r := binary.BigEndian.Uint32(bf)
	n := float64(math.Float32frombits(r))
	fmt.Println(r, n)

	assert.True(t, false)
}
