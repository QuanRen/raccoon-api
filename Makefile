build: clear
	go build -o ./build/bin/$(SER) ./service/$(SER)/main.go

run: build
	./build/bin/$(SER) -confpath ./conf/

clear:
	rm -rf ./build/bin/$(SER)
